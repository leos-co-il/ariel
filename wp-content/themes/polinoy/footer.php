<?php
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
$foo_img = get_field('contact_foo_img', $contact_id);
$names = ['youtube', 'pinterest', 'instagram', 'facebook', 'whatsapp'];
$socials = get_social_links($names);
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<span class="to-top-wrap">
				<img src="<?= ICONS ?>to-top.png">
			</span>
			<h3 class="top-text">חזרה למעלה</h3>
		</a>
		<div class="container footer-container-menu">
			<div class="row justify-content-between align-items-start">
				<?php if ($filial = opt('filial')) : ?>
					<div class="col-lg-2 col-sm-5 col-12">
						<?php foreach ($filial as $filial_item) : ?>
							<div class="foo-menu">
								<h3 class="foo-title">
									סניף <?= $filial_item['fil_name']; ?>
								</h3>
								<ul>
									<li>
										<a href="tel:<?= $filial_item['tel']; ?>">
											<?= $filial_item['tel']; ?>
										</a>
									</li>
									<li>
												<span>
													<?= $filial_item['fax']; ?>
												</span>
									</li>
									<li>
										<a href="https://waze.com/ul?q=<?= $filial_item['address']; ?>">
											<?= $filial_item['address']; ?>
										</a>
									</li>
									<li>
												<span>
													<?= $filial_item['open_hours']; ?>
												</span>
									</li>
								</ul>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
				<div class="col-xl-auto col-md-6 col-12 foo-menu foo-menu-links">
					<h3 class="foo-title">
						<?= opt('foo_menu_title'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '2', 'hop-hey two-columns'); ?>
					</div>
				</div>
				<?php if ($current_id !== $contact_id) : ?>
					<div class="col-xl-4 col-lg col-sm-7 col-12 foo-form-col">
						<div class="white-back-form white-opacity-form">
							<?php if ($logo_black = opt('logo')) : ?>
								<div class="logo mb-3">
									<img src="<?= $logo_black['url']; ?>">
								</div>
							<?php endif;
							if ($f_title = opt('foo_form_title')) : ?>
								<h2 class="form-title mb-2"><?= $f_title; ?></h2>
							<?php endif;
							getForm('6'); ?>
						</div>
					</div>
				<?php else: ?>
					<div class="col-lg-4 col-sm-7 col-12 foo-form-col">
					</div>
					<div class="contact-foo-img">
						<?php if ($foo_img && ($contact_id === $current_id)) : ?>
							<img src="<?= $foo_img['url']; ?>">
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
			<div class="row">
				<div class="col-12 foo-main-menu">
					<?php getMenu('footer-menu', '2'); ?>
				</div>
				<?php if ($socials) : ?>
					<div class="col-12 socials-footer">
						<?php foreach ($socials as $social_link) : ?>
							<a href="<?= !($social_link['name'] === 'whatsapp') ? $social_link['url'] : 'https://api.whatsapp.com/send?phone='.$social_link['url']; ?>" target="_blank" class="social-link">
								<i class="fab fa-<?= $social_link['name']; ?>"></i>
							</a>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo_black.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
    require_once THEMEPATH . "/inc/debug.php"
    ?>
    <script>

        function _fetchHeader($_el){
            let res = {
                'count' : 0,
                'content' : ''
            } ;
            $($_el).each(function () {
                res.count++;
                res.content += ' [' + $(this).text() + '] ';
            });
            return 'Count: ' + res.count + '. Text: ' + res.content;
        }

        function _fetchMeta($_meta){
            return $('meta[name='+$_meta+']').attr("content");
        }




        phpdebugbar.addDataSet({
            "SEO Local": {
                'H1' : _fetchHeader('h1'),
                'H2' : _fetchHeader('h2'),
                'H3' : _fetchHeader('h3'),
                'Meta Title' : _fetchMeta('title'),
                'Meta Description' : _fetchMeta('description'),
                'Meta Keywords' : _fetchMeta('keywords'),
            }
        });
    </script>

<?php endif; ?>

</body>
</html>
