<?php

get_header();
$query = get_queried_object();
$posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'tax_query' => array(
		array(
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
?>
<article class="page-product-body category-page-body pb-0">
	<div class="container prod-title-cont mt-0">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="block-title mb-3">Articles</h2>
			</div>
			<div class="col-lg-8 col-md-10 col-12 mb-3">
				<div class="home-main-output">
					<h2><?= $query->name; ?></h2>
					<?php category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($posts) : ?>
		<div class="home-categories-wrapper cat-block-layout">
			<?php foreach ($posts as $project) : $project_image = has_post_thumbnail($project) ? postThumb($project): '';?>
				<a class="home-category category-item" href="<?php the_permalink($project); ?>"
				   style="background-image: url('<?= $project_image; ?>')">
					<div class="category-overlay">
						<h2 class="home-category-title">
							<?= $project->post_title; ?>
						</h2>
						<div class="cat-content">
							<p class="cat-preview"><?= text_preview($project->post_content, '15'); ?></p>
							<img src="<?= ICONS ?>plus.png" alt="plus" class="plus-cat">
						</div>
					</div>
				</a>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</article>
<?php if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
		'img_back' => get_field('slider_img_back', $query),
	]);
}
get_footer(); ?>

