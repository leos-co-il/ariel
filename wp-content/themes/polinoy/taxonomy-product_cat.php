<?php
$query = get_queried_object();
get_header();
$cats = get_terms([
	'taxonomy' => 'product_cat',
	'hide_empty' => false,
	'parent' => 0,
]);
$samePosts = get_field('same_posts', $query) ? get_field('same_posts', $query) : get_posts([
	'posts_per_page' => 3,
	'post_type' => 'product',
]);
$products = get_posts([
	'numberposts' => -1,
	'post_type' => 'product',
	'tax_query' => array(
		array(
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$products_all = get_posts([
	'posts_per_page' => -1,
	'post_type' => 'product',
	'tax_query' => array(
		array(
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
?>
	<article class="page-product-body product-page">
		<div class="page-product-top">
		</div>
		<div class="container mt-0">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="block-title mb-3">PRODUCTS</h2>
				</div>
				<div class="col-lg-8 col-md-10 col-12 mb-3">
					<h2 class="post-page-title"><?= $query->name; ?></h2>
					<div class="slider-output post-content-output text-center">
						<?= category_description(); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-3 col-md-4 col-12">
					<div class="accordion" id="accordion-cats">
						<?php foreach($cats as $number => $parent_term) : ?>
							<?php $term_children = get_term_children($parent_term->term_id, 'product_cat'); ?>
							<div class="card">
								<div class="card-header" >
									<div class="card-header-wrapper">
										<a class="prod-cat-title" href="<?= get_category_link($parent_term); ?>">
											<?= $parent_term->name; ?>
										</a>
										<?php if (!empty($term_children)) : ?>
											<button class="btn btn-link accordion-trigger" type="button">
												<img src="<?= ICONS ?>plus-red.png" alt="plus">
											</button>
										<?php endif; ?>
									</div>
								</div>
								<div id="collapse-<?= $number; ?>" class="collapse body-acc">
									<div class="card-body">
										<?php if ($term_children) :
											foreach ($term_children as $x => $child) : $term = get_term_by( 'id', $child, 'product_cat' ); ?>
												<a class="child-link" href="<?= get_category_link($term); ?>">
													<?= $term->name; ?>
												</a>
											<?php endforeach;
										endif; ?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-lg-9 col-md-8 col-12">
					<?php if ($products) : ?>
						<div class="row justify-content-center align-items-stretch vacas">
							<?php foreach ($products as $number => $product) : ?>
								<?php get_template_part('views/partials/card', 'post_ajax',
									[
										'link_title' => 'מעבר למוצר',
										'post' => $product,
									]);
								?>
							<?php endforeach; ?>
						</div>
					<?php else: ?>
						<div class="row justify-content-center align-items-stretch">
							<div class="col-auto not-found-posts">
								<h3 class="post-page-title">אין מוצרים בקטגוריה</h3>
							</div>
						</div>
					<?php endif; ?>
					<?php if (count($products_all) > 9) : ?>
						<div class="row justify-content-center my-3">
							<div class="col-auto">
								<a class="base-link more-black-link load-more load-more-products">
									טען עוד תמונות
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="about-part-content same-block">
			<div class="form-about">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
			<?php if ($samePosts) : ?>
				<div class="same-posts-block">
					<div class="container-fluid">
						<div class="row justify-content-center">
							<div class="col-lg-11 col-12">
								<h2 class="same-block-title">YOU MIGHT ALSO LIKE</h2>
							</div>
							<div class="col-lg-11 col-12">
								<div class="row justify-content-center align-items-stretch">
									<?php foreach ($samePosts as $number => $post) : ?>
										<div class="col-lg-4 col-sm-10 col-12 post-item-col">
											<?php get_template_part('views/partials/card', 'post',
												[
													'link_title' => 'מעבר למוצר',
													'post' => $post,
												]);
											?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</article>
<?php if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => get_field('faq_title', $query),
			'block_text' => get_field('faq_text', $query),
			'faq' => $faq,
		]);
}
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
		'img_back' => get_field('slider_img_back', $query),
	]);
}
get_footer(); ?>
