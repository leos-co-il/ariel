<?php
the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$currentType = get_post_type($postId);
$images = [];
if ($fields['post_gallery'] || has_post_thumbnail()) {
	$images = makeGalleryArr($fields['post_gallery']);
}
$taxname = 'category';
$post_link = 'קרא עוד';
switch ($currentType) {
	case 'product':
		$taxname = 'product_cat';
		$post_link = 'מעבר למוצר';
		break;
	case 'post':
		$taxname = 'category';
		$post_link = 'מעבר למאמר';
		break;
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 3,
		'post_type' => $currentType,
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => $taxname,
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post_type' => $currentType,
			'post__not_in' => array($postId),
	]);
} ?>
	<article class="page-product-body">
		<div class="page-product-top"></div>
		<div class="container-fluid mt-0">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="post-page-title"><?php the_title(); ?></h2>
				</div>
				<?php if ($fields['product_desc']) : ?>
					<div class="col-xl-7 col-lg-8 col-md-10 col-12 mb-5">
						<p class="post-page-desc"><?= $fields['product_desc']; ?></p>
					</div>
				<?php endif; ?>
			</div>
			<div class="row product-row-main">
				<div class="col-md-11 col-12 product-body-col">
					<div class="row">
						<div class="col-lg-7 col-12">
							<div class="slider-output post-content-output">
								<?php the_content(); ?>
							</div>
						</div>
						<?php if ($images) : ?>
							<div class="col-lg-5 col-12 prod-arrows-slider">
								<div class="slider-product" dir="rtl">
									<?php foreach ($images as $image) : ?>
										<div>
											<div class="gallery-top-item"
												 style="background-image: url('<?= $image; ?>')">
											</div>
										</div>
									<?php endforeach; ?>
								</div>
								<?php if (count($images) > 1) : ?>
									<div class="slider-nav" dir="rtl">
										<?php foreach ($images as $image) : ?>
											<div class="slider-padding">
												<div class="gallery-list-item"
													 style="background-image: url('<?= $image;?>')">
												</div>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="about-part-content same-block">
			<div class="form-about">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
			<?php if ($samePosts) : ?>
				<div class="same-posts-block">
					<div class="container-fluid">
						<div class="row justify-content-center">
							<div class="col-lg-11 col-12">
								<h2 class="same-block-title">YOU MIGHT ALSO LIKE</h2>
							</div>
							<div class="col-lg-11 col-12">
								<div class="row justify-content-center align-items-stretch">
									<?php foreach ($samePosts as $number => $post) : ?>
										<div class="col-lg-4 col-sm-10 col-12 post-item-col">
											<?php get_template_part('views/partials/card', 'post',
													[
															'link_title' => $post_link,
															'post' => $post,
													]);
											?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</article>
<?php if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'block_text' => $fields['faq_text'],
			'faq' => $fields['faq_item'],
		]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
		'img_back' => $fields['slider_img_back'],
	]);
}
get_footer();
