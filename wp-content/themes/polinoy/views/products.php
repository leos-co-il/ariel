<?php
/*
Template Name: מוצרים
*/

get_header();
$fields = get_fields();
$cats = get_terms([
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'parent' => 0,
]);
$samePosts = get_posts([
		'posts_per_page' => 3,
		'post_type' => 'product',
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
}
$products = get_posts([
		'posts_per_page' => 9,
		'post_type' => 'product',
]);
$products_all = get_posts([
		'posts_per_page' => -1,
		'post_type' => 'product',
]);
?>
	<article class="page-product-body product-page">
		<div class="page-product-top">
		</div>
		<div class="container prod-title-cont mt-0">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="block-title mb-3">PRODUCTS</h2>
				</div>
				<div class="col-lg-8 col-md-10 col-12 mb-3">
					<h2 class="post-page-title"><?php the_title(); ?></h2>
					<div class="slider-output post-content-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-3 col-md-4 col-12">
						<div class="accordion" id="accordion-cats">
							<?php foreach($cats as $number => $parent_term) : ?>
								<?php $term_children = get_term_children($parent_term->term_id, 'product_cat'); ?>
								<div class="card">
									<div class="card-header" >
										<div class="card-header-wrapper">
											<a class="prod-cat-title" href="<?= get_category_link($parent_term); ?>">
												<?= $parent_term->name; ?>
											</a>
											<?php if (!empty($term_children)) : ?>
												<button class="btn btn-link accordion-trigger" type="button">
													<img src="<?= ICONS ?>plus-red.png" alt="plus" class="plus">
													<img src="<?= ICONS ?>minus-red.png" alt="minus" class="minus">
												</button>
											<?php endif; ?>
										</div>
									</div>
									<div id="collapse-<?= $number; ?>" class="collapse body-acc">
										<div class="card-body">
											<?php if ($term_children) :
												foreach ($term_children as $x => $child) : $term = get_term_by( 'id', $child, 'product_cat' ); ?>
													<a class="child-link" href="<?= get_category_link($term); ?>">
														<?= $term->name; ?>
													</a>
												<?php endforeach;
											endif; ?>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
					<div class="col-lg-9 col-md-8 col-12">
						<?php if ($products) : ?>
							<div class="row justify-content-center align-items-stretch vacas">
								<?php foreach ($products as $number => $product) :
									get_template_part('views/partials/card', 'post_ajax',
											[
													'link_title' => 'מעבר למוצר',
													'post' => $product,
											]);
									endforeach; ?>
							</div>
						<?php else: ?>
							<div class="row justify-content-center align-items-stretch">
								<div class="col-auto not-found-posts">
									<h3 class="post-page-title">אין מוצרים בקטגוריה</h3>
								</div>
							</div>
						<?php endif; ?>
						<?php if (count($products_all) > 9) : ?>
							<div class="row justify-content-center my-3">
								<div class="col-auto">
									<a class="base-link more-black-link load-more load-more-products">
										טען עוד תמונות
									</a>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<div class="about-part-content same-block">
			<div class="form-about">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
			<?php if ($samePosts) : ?>
				<div class="same-posts-block">
					<div class="container-fluid">
						<div class="row justify-content-center">
							<div class="col-lg-11 col-12">
								<h2 class="same-block-title">YOU MIGHT ALSO LIKE</h2>
							</div>
							<div class="col-lg-11 col-12">
								<div class="row justify-content-center align-items-stretch">
									<?php foreach ($samePosts as $number => $post) : ?>
										<div class="col-lg-4 col-sm-10 col-12 post-item-col">
											<?php get_template_part('views/partials/card', 'post',
												[
													'link_title' => 'מעבר למוצר',
													'post' => $post,
												]);
											?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</article>
<?php if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'block_text' => $fields['faq_text'],
			'faq' => $fields['faq_item'],
		]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
		'img_back' => $fields['slider_img_back'],
	]);
}
get_footer(); ?>
