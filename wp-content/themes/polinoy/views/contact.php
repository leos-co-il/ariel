<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

?>
<article class="contact-page">
	<div class="container">
		<?php if ($fields['contact_main_title']) : ?>
			<div class="row justify-content-center">
				<div class="col mb-4">
					<h2 class="block-title"><?= $fields['contact_main_title']; ?></h2>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center">
			<div class="col-lg-12 col-11">
				<div class="contact-page-form">
					<div class="row justify-content-center align-items-stretch">
						<div class="<?= $fields['contact_form_img'] ? 'col-lg-6 col-12' : 'col-12'; ?>
					d-flex align-items-center justify-content-center">
							<div>
								<?php if ($fields['contact_form_title']) : ?>
									<h2 class="form-title mb-2"><?= $fields['contact_form_title']; ?></h2>
								<?php endif;
								getForm('8'); ?>
							</div>
						</div>
						<?php if ($fields['contact_form_img']) : ?>
							<div class="col-lg-6 col-12 d-flex justify-content-center align-items-center contact-f-img">
								<img src="<?= $fields['contact_form_img']['url'];?>">
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<?php if ($filial = opt('filial')) : foreach ($filial as $x => $filial_item) : ?>
				<div class="col-lg-12 col-11 filial-col">
					<div class="row justify-content-center align-items-stretch filial-row">
						<div class="col-lg-6 <?= !($filial_item['fil_img']) ? 'col-12' : ''?>">
							<div class="filial-item">
								<h3 class="home-category-title text-right">
									סניף <?= $filial_item['fil_name']; ?>
								</h3>
								<ul>
									<li>
										<a href="https://waze.com/ul?q=<?= $filial_item['address']; ?>">
											<?= $x == 0 ? 'סניף הדגל: ' : 'סניף '.$filial_item['fil_name'].' :'; ?><?= $filial_item['address']; ?>
										</a>
									</li>
									<li>
										<a href="tel:<?= $filial_item['tel']; ?>">
											טלפון:<?= $filial_item['tel']; ?>
										</a>
									</li>
									<li>
										<span>
											פקס:<?= $filial_item['fax']; ?>
										</span>
									</li>
									<li>
										<span>
											שעות פתיחה:<?= $filial_item['open_hours']; ?>
										</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-6 col-12 cont-col-img" style="background-image: url('<?= $filial_item['fil_img']['url']; ?>')">
						</div>
					</div>
				</div>
			<?php endforeach; endif; ?>
		</div>
	</div>
</article>


<?php get_footer(); ?>
