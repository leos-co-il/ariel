<?php
/*
Template Name: קטגוריות פרויקטים
*/
get_header();
$fields = get_fields();
$cats = get_terms([
	'taxonomy' => 'project_cat',
	'hide_empty' => false,
	'parent' => 0,
]);
?>
<article class="page-product-body category-page-body">
	<div class="container prod-title-cont mt-0">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="block-title mb-3">Categories</h2>
			</div>
			<div class="col-lg-8 col-md-10 col-12 mb-3">
				<div class="home-main-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($cats) : ?>
	<div class="home-categories-wrapper cat-block-layout">
		<?php foreach ($cats as $cat_item) : $cat_image = get_field('cat_img', $cat_item);?>
		<a class="home-category category-item" href="<?= get_term_link((int)$cat_item->term_id); ?>"
		   style="background-image: url('<?= $cat_image['url']; ?>')">
			<div class="category-overlay">
				<h2 class="home-category-title">
					<?= $cat_item->name; ?>
				</h2>
				<div class="cat-content">
					<p class="cat-preview"><?= text_preview(category_description($cat_item), '15'); ?></p>
					<img src="<?= ICONS ?>plus.png" alt="plus" class="plus-cat">
				</div>
			</div>
		</a>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
</article>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
		'img_back' => $fields['slider_img_back'],
	]);
}
get_footer(); ?>

