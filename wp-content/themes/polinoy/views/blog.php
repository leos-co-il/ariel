<?php
/*
Template Name: מאמרים
*/
get_header();
$fields = get_fields();
$type = $fields['type_of_post'] ? $fields['type_of_post'] : 'post';
$title = ($fields['type_of_post'] == 'project') ? 'Projects' : 'Articles';
$projects = get_posts([
	'numberposts' => -1,
	'post_type' => $type,
]);
?>
<article class="page-product-body category-page-body pb-0">
	<div class="container prod-title-cont mt-0">
		<div class="row justify-content-center">
			<div class="col-12">
				<h2 class="block-title mb-3"><?= $title; ?></h2>
			</div>
			<div class="col-lg-8 col-md-10 col-12 mb-3">
				<div class="home-main-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($projects) : ?>
		<div class="home-categories-wrapper cat-block-layout">
			<?php foreach ($projects as $project) : $project_image = has_post_thumbnail($project) ? postThumb($project): '';?>
				<a class="home-category category-item" href="<?php the_permalink($project); ?>"
				   style="background-image: url('<?= $project_image; ?>')">
					<div class="category-overlay">
						<h2 class="home-category-title">
							<?= $project->post_title; ?>
						</h2>
						<div class="cat-content">
							<p class="cat-preview"><?= text_preview($project->post_content, '15'); ?></p>
							<img src="<?= ICONS ?>plus.png" alt="plus" class="plus-cat">
						</div>
					</div>
				</a>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</article>
<?php
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_text' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
		'img_back' => $fields['slider_img_back'],
	]);
}
get_footer(); ?>

