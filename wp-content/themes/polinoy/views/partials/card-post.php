<?php if (isset($args['post']) && $args['post']) : ?>
	<div class="post-item more-card" <?php if (has_post_thumbnail($args['post'])) : ?>
		style="background-image: url('<?= postThumb($args['post']); ?>')"
	<?php endif;?> data-id="<?= $args['post']->ID; ?>">
		<span class="post-item-overlay">
			<div class="post-item-content">
				<a class="post-item-title" href="<?php the_permalink($args['post']); ?>"><?= $args['post']->post_title; ?></a>
				<p class="post-item-text mb-3">
					<?= text_preview($args['post']->post_content, 8); ?>
				</p>
				<a href="<?php the_permalink($args['post']); ?>" class="base-link white-link">
					<?= (isset($args['link_title']) && $args['link_title']) ? $args['link_title'] : 'קרא עוד';?>
				</a>
			</div>
		</span>
	</div>
<?php endif; ?>
