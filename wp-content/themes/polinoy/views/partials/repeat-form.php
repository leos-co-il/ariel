<div class="repeat-form-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-9 col-lg-11 col-md-10 col-sm-11 col-12 white-back-form white-opacity-form repeat-form">
				<div class="row align-items-stretch justify-content-center w-100 about-form-row">
					<?php if ($rep_title = opt('mid_form_title')) : ?>
						<div class="col-auto d-flex align-items-center mb-2">
							<h2 class="form-title-big"><?= $rep_title; ?></h2>
						</div>
					<?php endif; ?>
					<?php if ($rep_text = opt('mid_form_subtitle')) : ?>
						<div class="col-auto d-flex align-items-center mb-2">
							<h3 class="form-title">
								<?= $rep_text; ?>
							</h3>
						</div>
					<?php endif; ?>
					<div class="col-12 middle-form wow zoomIn">
						<?php getForm('7'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
