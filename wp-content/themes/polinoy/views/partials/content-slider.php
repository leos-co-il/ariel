<?php if (isset($args['content']) && $args['content']) : ?>
	<section class="slider-base-wrap arrows-slider" <?php if (isset($args['img_back']) && $args['img_back']) : ?>
		style="background-image: url('<?= $args['img_back']['url']; ?>')"
		<?php endif; ?>>
		<div class="slider-overlay"></div>
		<div class="container slider-container">
			<div class="row justify-content-center align-items-center">
				<div class="col-lg-6 col-12 <?= (isset($args['img']) && $args['img']) ? '': 'col-lg-12'; ?> slider-wrap-col">
					<div class="slider-text-wrap">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['content'] as $content) : ?>
								<div>
									<div class="slider-output"><?= $content['content']; ?></div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
				<?php if (isset($args['img']) && $args['img']) : ?>
					<div class="col-lg-6 d-flex justify-content-center align-items-center">
						<img src="<?= $args['img']['url']; ?>" class="slider-img">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
