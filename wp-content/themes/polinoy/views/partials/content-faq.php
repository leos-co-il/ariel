<?php if (isset($args['faq'])) : ?>
	<section class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="home-main-output">
						<h2>
							<?= (isset($args['block_title']) && $args['block_title']) ? $args['block_title'] : 'שאלות נפוצות'; ?>
						</h2>
						<p><?= (isset($args['block_text']) && $args['block_text']) ? $args['block_text'] : ''; ?>
						</p>
					</div>
				</div>
				<div class="col-12">
					<div id="accordion">
						<?php foreach ($args['faq'] as $num => $item) : ?>
							<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
								<div class="question-header" id="heading_<?= $num; ?>">
									<button class="btn question-title" data-toggle="collapse"
											data-target="#faqChild<?= $num; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span><?= $item['faq_question']; ?></span>
									</button>
									<div id="faqChild<?= $num; ?>" class="collapse faq-item"
										 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
										<div class="slider-output faq-output">
											<?= $item['faq_answer']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
