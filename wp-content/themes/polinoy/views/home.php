<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="home-main" <?php if ($fields['main_back_img']) : ?>
	style="background-image: url('<?= $fields['main_back_img']['url']; ?>')"
	<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-center">
			<?php if ($fields['main_text']) : ?>
				<div class="col-xl-8 col-lg-10 col-12">
					<div class="home-main-output">
						<?= $fields['main_text']; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<div class="row justify-content-center">
			<?php if ($fields['main_link']) : ?>
				<div class="col-auto">
					<a class="base-link" href="<?= (isset($fields['main_link']['url']) && $fields['main_link']['url']) ?
							$fields['main_link']['url'] : ''; ?>">
						<?= (isset($fields['main_link']['title']) && $fields['main_link']['title']) ?
								$fields['main_link']['title'] : 'עוד על הסיפור שלנו'; ?>
					</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php if ($fields['home_prod_cats']) : ?>
	<div class="home-categories-wrapper">
		<?php foreach ($fields['home_prod_cats'] as $cat_item) :
			$cat_image = get_field('cat_img', $cat_item); ?>
			<a class="home-category" href="<?= get_term_link((int)$cat_item->term_id); ?>"
			   style="background-image: url('<?= $cat_image['url']; ?>')">
				<div class="category-overlay">
					<?php if ($cat_icon = get_field('cat_icon', $cat_item)) : ?>
						<div class="cat-icon">
							<img src="<?= $cat_icon['url']; ?>">
						</div>
					<?php endif; ?>
					<h2 class="home-category-title">
						<?= $cat_item->name; ?>
					</h2>
				</div>
			</a>
		<?php endforeach;
		if ($fields['home_prod_cats_link']) : ?>
			<div class="home-category h-cats-back" <?php if ($fields['h_prod_cats_link_back']) : ?>
			style="background-image: url('<?= $fields['h_prod_cats_link_back']['url']; ?>')"
			<?php endif; ?>>
				<div class="category-overlay">
					<a class="base-link white-link" href="<?= $fields['home_prod_cats_link']['url']; ?>" >
						<?= (isset($fields['home_prod_cats_link']['title']) && $fields['home_prod_cats_link']['title']) ?
								$fields['home_prod_cats_link']['title'] : 'קטגוריות נוספות'; ?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>
<div class="container">
    <div class="row">
        <div class="col-12">
        </div>
    </div>
</div>

<section class="marmo-back">
	<?php get_template_part('views/partials/repeat', 'form'	);
	if ($fields['home_projects'] && $fields['home_projects_title']) : ?>
		<div class="container mt-5">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title"><?= $fields['home_projects_title']; ?></h2>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php if ($fields['home_projects']) : $chunked_projects = array_chunk($fields['home_projects'], 2); ?>
	<div class="home-projects">
		<?php foreach ($chunked_projects as $y => $pr_col) : ?>
			<div class="project-col">
				<?php foreach ($pr_col as $x => $project_item) : ?>
					<div class="project-item">
						<div class="project-image" <?php if (has_post_thumbnail($project_item)) : ?>
							style="background-image: url('<?= postThumb($project_item); ?>')"
						<?php endif;?>>
							<span class="post-item-overlay">
								<div class="post-item-content">
									<a class="post-item-title" href="<?php the_permalink($project_item); ?>">
										<?= $project_item->post_title; ?>
									</a>
									<p class="post-item-text mb-3">
										<?= text_preview($project_item->post_content, 8); ?>
									</p>
									<a href="<?php the_permalink($project_item); ?>" class="base-link">
										לפרוייקט המלא
									</a>
								</div>
							</span>
						</div>
					</div>
				<?php if (($y === 2)  && $fields['home_projects_link']) : ?>
						<div class="project-item project-item-link">
							<div class="project-image project-link-image">
								<div class="post-item-overlay">
									<a class="base-link white-link" href="<?= $fields['home_projects_link']['url']; ?>" >
										<?= (isset($fields['home_projects_link']['title']) && $fields['home_projects_link']['title']) ?
												$fields['home_projects_link']['title'] : 'פרוייקטים נוספים'; ?>
									</a>
								</div>
							</div>
						</div>
					<?php endif;
				endforeach; ?>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif;
if ($fields['home_content_slider']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['home_content_slider'],
			'img' => $fields['h_slider_img'],
			'img_back' => $fields['h_slider_back_img'],
	]);
}
?>
<section class="marmo-back pb-0">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-11 col-md-10 col-sm-11 col-12 repeat-form">
				<div class="row align-items-stretch justify-content-center">
					<?php if ($fields['h_form_title']) : ?>
						<div class="col-auto d-flex align-items-center mb-2">
							<h2 class="form-title-big"><?= $fields['h_form_title']; ?></h2>
						</div>
					<?php endif; ?>
					<?php if ($fields['h_form_subtitle']) : ?>
						<div class="col-auto d-flex align-items-center mb-2">
							<h3 class="form-title">
								<?= $fields['h_form_subtitle']; ?>
							</h3>
						</div>
					<?php endif; ?>
					<div class="col-12 middle-form wow zoomIn">
						<?php getForm('7'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>	<?php if ($fields['h_posts_slider']) : ?>
		<div class="home-posts-block">
			<?php if ($fields['h_posts_title']) : ?>
				<div class="container mt-5">
					<div class="row">
						<div class="col-12 mb-4">
							<h2 class="block-title"><?= $fields['h_posts_title']; ?></h2>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<div class="posts-slider-wrap">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-xl-10 col-md-11 col-12">
							<div class="row justify-content-center align-items-center">
								<div class="col-md-6 col-12 big-slider-col">
									<div class="post-slider-big" dir="rtl">
										<?php foreach ($fields['h_posts_slider'] as $post_slide) : ?>
											<div>
												<div class="gallery-top-item post-top-item" <?php if (has_post_thumbnail($post_slide)) : ?>
													style="background-image: url('<?= postThumb($post_slide); ?>')"
												<?php endif; ?>>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
									<?php if ($fields['h_posts_link']) : ?>
										<a class="base-link base-link-large" href="<?= (isset($fields['h_posts_link']['url']) && $fields['h_posts_link']['url']) ?
												$fields['h_posts_link']['url'] : ''; ?>">
											<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title']) ?
													$fields['h_posts_link']['title'] : 'לכל המאמרים'; ?>
										</a>
									<?php endif; ?>
								</div>
								<div class="col-md-6 col-12 post-slider-col prod-arrows-slider proj-arrows-slider">
									<div class="post-slider-preview" dir="rtl">
										<?php foreach ($fields['h_posts_slider'] as $post_slide) : ?>
											<div>
												<div class="post-preview">
													<h3 class="preview-post-title"><?= $post_slide->post_title; ?></h3>
													<p class="base-preview"><?= text_preview($post_slide->post_content, '10'); ?></p>
													<a href="<?php the_permalink($post_slide);?>" class="base-preview">קראו עוד</a>
												</div>
											</div>
										<?php endforeach; ?>
									</div>
									<?php if (count($fields['h_posts_slider']) > 1) : ?>
										<div class="post-slider-nav" dir="rtl">
											<?php foreach ($fields['h_posts_slider'] as $post_slide) : ?>
												<div class="slider-padding">
													<div class="gallery-list-item"<?php if (has_post_thumbnail($post_slide)) : ?>
														style="background-image: url('<?= postThumb($post_slide); ?>')"
													<?php endif; ?>>
													</div>
												</div>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<?php if ($fields['h_posts_link']) : ?>
								<div class="row blog-link-row">
									<div class="col-auto align-self-start">
										<a class="base-link" href="<?= (isset($fields['h_posts_link']['url']) && $fields['h_posts_link']['url']) ?
												$fields['h_posts_link']['url'] : ''; ?>">
											<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title']) ?
													$fields['h_posts_link']['title'] : 'לכל המאמרים'; ?>
										</a>
									</div>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
			[
					'block_title' => $fields['faq_title'],
					'block_text' => $fields['faq_text'],
					'faq' => $fields['faq_item'],
			]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
			'img_back' => $fields['slider_img_back'],
	]);
}
get_footer(); ?>
