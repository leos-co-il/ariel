<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="about-page-body">
	<div class="about-page-main" <?php if (has_post_thumbnail()) : ?>
		style="background-image: url('<?= postThumb(); ?>')"
	<?php endif; ?>>
		<div class="container mt-5">
			<div class="row">
				<div class="col-12">
					<h2 class="block-title mb-4"><?= $fields['about_page_title'] ? $fields['about_page_title'] : 'About us'; ?></h2>
				</div>
				<?php if ($fields['about_page_text']) : ?>
					<div class="col-12">
						<div class="home-main-output about-text-top">
							<?= $fields['about_page_text']; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="about-content-page">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-9 col-lg-10 col-12">
					<div class="home-main-output">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['about_benefits']) : ?>
		<div class="about-benefits-block">
			<?php foreach ($fields['about_benefits'] as $num => $benefit) : ?>
				<div class="benefit-row wow zoomIn" data-wow-delay="0.<?= $num * 2; ?>s">
					<div class="item-col-50 benefit-content-col <?= $fields['about_benefits'] ? '' : 'max-content-wrap'; ?>">
						<div class="about-item-content">
							<?= $benefit['benefit_text']; ?>
						</div>
					</div>
					<?php if ($benefit['benefit_img']) : ?>
						<div class="item-col-50 cont-col-img" style="background-image: url('<?= $benefit['benefit_img']['url']; ?>')">
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif;
	if ($fields['about_item_img'] || $fields['about_item_text']) : ?>
		<div class="about-part-content">
			<div class="container">
				<div class="row justify-content-center align-items-end position-relative">
					<?php if ($fields['about_item_text']) : ?>
						<div class="slider-output about-part-output">
							<?= $fields['about_item_text']; ?>
						</div>
					<?php endif; ?>
					<div class="col-lg-10 col-12">
						<div class="row justify-content-start">
							<?php if ($fields['about_item_img']) : ?>
								<div class="col-xl-7 col-lg-8 col-md-10 col-12">
									<img src="<?= $fields['about_item_img']['url']; ?>">
								</div>
							<?php endif;
							if ($fields['about_item_text']) : ?>
								<div class="col-12 mt-4 mobile-about-col slider-output">
									<?= $fields['about_item_text']; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<div class="form-about">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
		]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
			'img_back' => $fields['slider_img_back'],
	]);
}
get_footer(); ?>
