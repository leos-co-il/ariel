<?php
the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$post_link = 'מעבר לפרוייקט';
$post_terms = wp_get_object_terms($postId, 'project_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 3,
	'post_type' => 'project',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'project_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 3,
		'orderby' => 'rand',
		'post_type' => 'project',
		'post__not_in' => array($postId),
	]);
} ?>
	<article class="page-product-body">
		<div class="page-product-top ">
		</div>
		<div class="container mt-0">
			<div class="row justify-content-center">
				<div class="col-12">
					<h2 class="block-title mb-3">PROJECT GALLERY</h2>
				</div>
				<div class="col-lg-8 col-md-10 col-12 mb-3">
					<h2 class="post-page-title"><?php the_title(); ?></h2>
					<div class="slider-output post-content-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ($fields['post_gallery']) : ?>
			<div class="container-fluid" id="gallery-block">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['post_gallery'] as $photo) : ?>
						<div class="col-xl-3 col-lg-4 col-sm-6 col-12 mb-3 project-item-col">
							<a class="project-photo" style="background-image: url('<?= $photo['url']; ?>')"
							href="<?= $photo['url']; ?>" data-lightbox="project-gallery"></a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif;
		if (count($fields['post_gallery']) > 12) : ?>
			<div class="container my-3">
				<div class="row justify-content-center">
					<div class="col-auto">
						<a class="base-link more-black-link load-more" id="load-more-items">
							טען עוד תמונות
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="about-part-content same-block">
			<div class="form-about">
				<?php get_template_part('views/partials/repeat', 'form'); ?>
			</div>
			<?php if ($samePosts) : ?>
				<div class="same-posts-block">
					<div class="container-fluid">
						<div class="row justify-content-center">
							<div class="col-lg-11 col-12">
								<h2 class="same-block-title">YOU MIGHT ALSO LIKE</h2>
							</div>
							<div class="col-lg-11 col-12">
								<div class="row justify-content-center align-items-stretch">
									<?php foreach ($samePosts as $number => $post) : ?>
										<div class="col-lg-4 col-sm-10 col-12 post-item-col">
											<?php get_template_part('views/partials/card', 'post',
												[
													'link_title' => $post_link,
													'post' => $post,
												]);
											?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</article>
<?php if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq',
		[
			'block_title' => $fields['faq_title'],
			'block_text' => $fields['faq_text'],
			'faq' => $fields['faq_item'],
		]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
		'img_back' => $fields['slider_img_back'],
	]);
}
get_footer();
