(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	//Load more items
	var sizeLi = $('#gallery-block .project-item-col').size();
	var x = 12;
	$('#gallery-block .project-item-col:lt('+x+')').addClass('show');
	$('#load-more-items').click(function () {
		x = (x + 4 <= sizeLi) ? x + 4 : sizeLi;
		$('#gallery-block .project-item-col:lt('+x+')').addClass('show');
		if (x === sizeLi) {
			$('#load-more-items').addClass('hide');
		}
	});
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.menu-item-has-children').click(function () {
			$(this).children('a').toggleClass('red-color');
			$(this).children('.sub-menu').toggleClass('show-menu');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').addClass('arrow-bottom');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').removeClass('arrow-bottom');
		});
		$('.slider-product').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			rtl: true,
			fade: true,
			asNavFor: '.slider-nav',
		});
		$('.slider-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.slider-product',
			arrows: true,
			dots: false,
			rtl: true,
			centerPadding: '0',
			centerMode: true,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 560,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$('.post-slider-preview').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			rtl: true,
			fade: true,
			asNavFor: '.post-slider-nav',
		});
		$('.post-slider-big').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			rtl: true,
			fade: true,
			asNavFor: '.post-slider-nav',
		});
		$('.post-slider-nav').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: '.post-slider-big, .post-slider-preview',
			arrows: true,
			dots: false,
			rtl: true,
			centerPadding: '50px',
			centerMode: false,
			focusOnSelect: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		var accordionCats = $('#accordion-cats');
		accordionCats.collapse({
			toggle: false
		});
		$('.accordion-trigger').on('click',function(){
			$(this).parent('.card-header-wrapper').parent('.card-header').parent('.card').children('.body-acc').toggle('show');
			$(this).children('.plus').toggleClass('hide');
			$(this).children('.minus').toggleClass('show');
		});
	});
	//More posts
	$('.load-more-products').click(function() {
		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});

		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				ids: ids,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.more-black-link').addClass('hide');
				}
				$('.vacas').append(data.html);
			}
		});
	});


})( jQuery );
