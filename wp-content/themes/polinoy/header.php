<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>

<?php
$current_id = get_the_ID();
$cats_id = getPageByTemplate('views/categories.php');
$cats_pr_id = getPageByTemplate('views/categories-projects.php');
$names = ['pinterest', 'instagram', 'facebook', 'youtube'];
$socials = get_social_links($names);
?>
<header class="sticky <?= (is_front_page() || ($cats_id === $current_id) || ($cats_pr_id === $current_id) ||
		(is_tax('project_cat')) || (is_tax('category'))) ? 'home-header' : '';?>">
	<div class="drop-menu">
		<nav class="drop-nav">
			<?php getMenu('header-menu', '2', '', ''); ?>
			<?php if ($socials) : ?>
				<div class="socials-footer header-socials">
					<?php foreach ($socials as $social_link) : ?>
						<a href="<?= $social_link['url']; ?>" target="_blank" class="social-link">
							<i class="fab fa-<?= $social_link['name']; ?>"></i>
						</a>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</nav>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row justify-content-between align-items-center">
					<div class="col-xl-5 col-sm col-2 d-flex justify-content-start align-items-center position-relative">
						<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
						</button>
					</div>
					<?php if ($logo = opt('logo_white')) : ?>
						<div class="col-xl col-sm-4 col-8">
							<a href="/" class="logo">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						</div>
					<?php endif; ?>
					<div class="col-xl-5 col-sm col-2 d-flex align-items-center justify-content-end">
						<?php if ($tel = opt('tel')) : ?>
							<a href="tel:<?= $tel; ?>" class="header-tel d-flex justify-content-center align-items-center">
								<span class="tel-number">
									<?= $tel; ?>
								</span>
								<img src="<?= ICONS ?>header-tel.png">
							</a>
						<?php endif; ?>
						<div class="pop-trigger">
							<img src="<?= ICONS ?>pop-trigger.png" alt="open-popup">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-5 col-lg-6 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form white-back-form">
					<span class="close-form">
						<img src="<?= ICONS ?>close.png">
					</span>
					<?php if ($logo_black = opt('logo')) : ?>
						<div class="logo">
							<img src="<?= $logo_black['url']; ?>">
						</div>
					<?php endif;
					if ($f_title = opt('pop_form_title')) : ?>
						<h2 class="form-title"><?= $f_title; ?></h2>
					<?php endif;
					getForm('6'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
