<?php Namespace WordPress\Plugin\GalleryManager;

abstract class WP_Query_Extensions {

  static function init(){
    #add_Filter('query_vars', [static::class, 'registerQueryVars']);
    add_Action('pre_get_posts', [static::class, 'filterAttachmentQuery']);
    add_Filter('posts_where', [static::class, 'filterWhereStmt'], 10, 2);
    add_Filter('posts_join', [static::class, 'filterJoinStmt'], 100, 2);
    add_Filter('ajax_query_attachments_args', [static::class, 'restrictAttachments']);
  }

  /*
  public static function registerQueryVars($query_vars){
    $query_vars[] = 'restrict_gallery_images';
    return $query_vars;
  }
  */

  static function filterAttachmentQuery($query){
    if (is_Admin() && $query->get('post_type') == 'attachment' && $query->get('orderby') == 'menu_order ASC, ID' && $query->get('order') == 'DESC')
      $query->set('order', 'ASC');
  }

  public static function filterJoinStmt($stmt, $query){
    global $wpdb;

    if ($gallery_id = (int) $query->get('restrict_gallery_images')){
      $post_type_name = Post_Type::post_type_name;
      $stmt .= " LEFT JOIN {$wpdb->posts} as galleries ON ({$wpdb->posts}.post_type = 'attachment' AND galleries.post_type = '{$post_type_name}' AND {$wpdb->posts}.post_parent = galleries.ID) ";
    }

    return $stmt;
  }

  public static function filterWhereStmt($stmt, $query){
    if ($gallery_id = (int) $query->get('restrict_gallery_images'))
      $stmt .= " AND (galleries.ID IS NULL OR galleries.ID = {$gallery_id}) ";

    return $stmt;
  }

  static function restrictAttachments($query_args){
    setType($query_args, 'Array');
    $user_id = get_Current_User_ID();
    $gallery_id = empty($_POST['post_id']) ? False : IntVal($_POST['post_id']);

    if ($gallery_id && Post::isGallery($gallery_id)){
      # Restrict attachments to their owner
      if ($user_id && !current_User_Can('edit_others_posts') && empty($query_args['author'])){
        $query_args['author'] = $user_id;
      }

      $query_args['restrict_gallery_images'] = $gallery_id;
    }

    return $query_args;
  }

}

WP_Query_Extensions::init();
